package edu.sjsu.android.exercise4tonyxie;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import edu.sjsu.android.exercise4tonyxie.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity implements SensorEventListener {

    private ActivityMainBinding binding;
    private SensorManager manager;
    private Sensor acc;
    private long lastUpdate = System.currentTimeMillis();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        manager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        if (manager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER) != null){
            acc = manager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        }
        else{
            binding.result.setText("Missing accelerometer");
        }
    }

    public void onSensorChanged(SensorEvent event) {
        if(event.sensor.getType() == Sensor.TYPE_ACCELEROMETER){
            // Your code here
            float x = event.values[0];
            float y = event.values[1];
            float z = event.values[2];
            String result = String.format("Acceleration:\n" +
                            "X axis: %.5f\nY axis: %.5f\nZ axis: %.5f",
                    x, y, z);
            binding.result.setText(result);

            float accSquareRoot = (x * x + y * y + z * z)
                    / (SensorManager.GRAVITY_EARTH * SensorManager.GRAVITY_EARTH);
            long actualTime = System.currentTimeMillis();
            if (accSquareRoot >= 2 && actualTime - lastUpdate > 200) {
                lastUpdate = actualTime;
                Toast.makeText(this, "Don't shake me!", Toast.LENGTH_LONG).show();
                binding.Text.setBackgroundColor(Color.RED);
            } else {
                binding.Text.setBackgroundColor(Color.BLUE);
            }
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    @Override
    protected void onStart() {
        super.onStart();
        if(acc != null)
            manager.registerListener(this, acc, SensorManager.SENSOR_DELAY_UI);
    }

    @Override
    protected void onStop() {
        super.onStop();
        manager.unregisterListener(this);
    }
}